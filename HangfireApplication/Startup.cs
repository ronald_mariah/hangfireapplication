﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Hangfire;
using Hangfire.SqlServer;

[assembly: OwinStartup(typeof(HangfireApplication.Startup))]

namespace HangfireApplication
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("HangfireDbConnectionString");
            
            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }
    }
}
