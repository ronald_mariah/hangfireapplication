﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hangfire;
using HangfireApplication.Models;

namespace HangfireApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            RecurringJob.AddOrUpdate(() => new BackgroundProcess().Process(), Cron.MinuteInterval(1));
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}